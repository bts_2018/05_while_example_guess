import java.util.Random;
import java.util.Scanner;

public class main {

	public static void main(String[] args) {

		int numbertoGuess;
		int guess=0;
		boolean win = false;
		int numberoftries=0;

		//random number
		Random xxx = new Random();
		numbertoGuess = xxx.nextInt(100);

		Scanner input = new Scanner(System.in);
		while (win == false) { 

			numberoftries++;

			//receive input
			System.out.println("Guess a number between 1 to 100: ");
			//guess = input.nextInt();

			try{
				guess = input.nextInt();
			} catch(Exception ex) {
				System.out.println("wronge input number, EXIT !");
				break;
			} 

			if (guess == numbertoGuess) {
				System.out.println("WIN IN " +  numberoftries + " TRIES");
				win = true;
			}else if (guess < numbertoGuess){
				System.out.println("\nYour gess is too low ");
			}else if (guess > numbertoGuess){
				System.out.println("\nYour gess is too high ");
			}
		}

		input.close();

	}

}
